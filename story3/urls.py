from django.urls import path
from . import views

urlpatterns = [
    path('', views.story3),
    path('quotes', views.story3_2)
]