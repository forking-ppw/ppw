from django.shortcuts import render


# Create your views here.

def story3(request):
    return render(request, 'story3.html')

def story3_2(request):
    return render(request, 'story3-2.html')